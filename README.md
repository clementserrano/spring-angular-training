# Spring Angular Training

This repository contains a quick setup in Gitpod for a basic app build w/ Spring and Angular.

Basic architecture for CRUD operations is already put in place : 
* Abstract classes for controller, services, repositories, factories, entities and DTOs
* One entity collaborator with all its related classes
* 2 pages : management and consultation and their components list and form

Some preferences in architecture : 
* Backend uses Spring JPA to create DB requests
* Frontend uses FormBuilder to create Angular ReactiveForms

You can use this repo to test some architecture change on a smaller app or to test your skills.

To access the app, you need to go on "Ports" tab, then click on "Open Browser" for port 4200. See [`Troubleshooting`](#troubleshooting) section if you encounter problems.

## Exercises

Below here are examples of exercises to test your skills. 

You can fork the project and push a commit for each exercice. Then create a merge request to express feedbacks 😊.

You will find the "Gitpod" button in Gitlab, under "Web IDE" with the select arrow. You will be prompted to give authorizations to Gitpod on your Gitlab account, and give phone number to receive a verification code. 

When pushing your changes, you will be prompt to add read_repository permission to Gitpod. Follow the link in the prompt, and edit permissions to add read_repository.

Don't stage application.properties and environment.ts changes made on workspace start up. They replace placeholders for backend and frontend URL. Replacing them would break their auto-configuration when creating a new workspace.

### Get used to the existing code *(20 min)*

### Exercice 1 *(5 min)*

In collaborators management page, "Disable form" check button should disable the form inputs.

### Exercice 2 *(7 min)*

Create an endpoint in backend, that returns collaborators that are managers.

### Exercice 3 *(7 min)*

When modifying a collaborator in management page (click on list item), the `<select>` for manager in the form shouldn't show the selected collaborator in the list of options.

### Exercice 4 *(7 min)*

When creating a new collaborator in management page, the collaborator created should be added to the list.

### Exercice 5 *(12 min)*

Collaborators consultation page should only display active collaborators. 

A parameter can be passed to the endpoint, with the method getAllFilter() in Angular CollaboratorsService.

A request param for all params is already read in the backend, but is not used for the database request.

### Exercice 6 *(15 min)*

Add an endpoint in /collaborators, to display a tree of managed collaborators (only on API side).

Starting with the collaborators without manager and descending in the children (managed).

You can choose the appropriate name of the endpoint.

### Exercice 7 *(12 min)*

Add in collaborator form, a field "Trigram" that updates on changes from input "firstname" and "lastname" to take : 

1st letter of firstname then 1st and 2nd letters of lastname. All in uppercase.

For example : 
* firstname: **C**lément
* lastname: **Se**rrano
* trigram : **CSE**

Trigram field doesn't need to be in an `<input>`, and doesn't need to be persisted in backend.

### Exercice 8 *(20 min)*

CollaboratorsListComponent has a sort function for columns ID, firstname, lastname and manager.

It is triggered by clicking on the corresponding table headers (sort icons are not necessary).

Implement the sort mechanism, and keep in mind that we might use it elsewhere in the application.

The sort mechanism should only compare the string values (no special comparison depending on the type).

### Exercice 9 *(20 min)*

**Requires Exercice 2**

In collaborators management page, create a `<select>` to filter collaborators list by manager, and fill it with previously created endpoint.

Add an option "All" to display all collaborators.

Do a console.log when both collaborators & managers list are fetched from backend.

# Evolution of this project

Next step will be to implement QueryDSL to better build requests.

And maybe add some Authentication using SAML or another related Identity Provider.

## Launch in localhost

If you really don't want to use Gitpod, you need to replace url in front and back properties file : 

* `backend-spring/src/main/resources/application.properties`
* `frontend-angular/src/environments/environment.ts`

Replace : 

* `#{backend_url}` with `localhost:8080` (Spring default port) 
* `#{frontend_url}` with `localhost:4200` (Angular default port)

Workspace version : 

* Java : openjdk 11.0.16 2022-07-19 LTS
* Nodejs : v16.17.1

Install dependencies : 

* In `backend-spring`, launch `./mvnw install -DskipTests=true`
* In `frontend-angular`, launch `npm install`
  * You will need @angular/cli installed in global : `npm install -g @angular/cli`

Then launch applications with : 

* In `backend-spring`, launch `./mvnw spring-boot:run`
* In `frontend-angular`, launch `npm start`

## Troubleshooting

If the application doesn't start or is blocked by CORS policy (backend), restarting the application could solve the problem : 

* `npm start` in /workspace/spring-angular-training/frontend-angular
  * And if it still doesn't work, reinstall dependencies : 
    * `npm install`
    * `npm install -g @angular/cli` to reinstall @angular/cli
* `./mvnw spring-boot:run` in /workspace/spring-angular-training/backend-spring
  * And if it still doesn't work, reinstall dependencies :
    * `./mvnw install -DskipTests=true`

If port exposition didn't work, you can right-click and "Retry to expose" in the "Ports" tab. 

If it stills doesn't work, try accessing the port by copying the workspace url and adding "{port}-" before. For example : 
* workspace : https://clementserr-springangul-x9zkaeco5hw.ws-eu64.gitpod.io/ 
* Angular port : https://4200-clementserr-springangul-x9zkaeco5hw.ws-eu64.gitpod.io/

On workspace startup, environment variables are replaced depending on workspace url.

If this replacement didn't work, check `frontend` and `backend` variables in src/main/resources/application.properties and src/environments/environment.ts. 

Replace frontend with result of command `gp url 4200` and backend with `gp url 8080`.
