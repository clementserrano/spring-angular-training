package com.example.demo.factories;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dtos.CollaboratorDTO;
import com.example.demo.services.CollaboratorService;
import com.example.demo.entities.Collaborator;

@Component
public class CollaboratorFactory implements AbstractFactory<Collaborator, CollaboratorDTO> {

    @Autowired
    private CollaboratorService service;

    @Override
    public Collaborator dtoToEntity(CollaboratorDTO dto) {
        Collaborator entity;
        if (dto.getId() != null) {
            entity = service.get(dto.getId()).orElseThrow(() -> new EntityNotFoundException());
        } else {
            entity = new Collaborator();
        }
        entity.setLastname(dto.getLastname());
        entity.setFirstname(dto.getFirstname());

        if (dto.getManagerId() != null) {
            entity.setManager(service.get(dto.getManagerId()).orElseThrow(() -> new EntityNotFoundException()));
        } else {
            entity.setManager(null);
        }

        entity.setActive(dto.getActive());
        return entity;
    }

    @Override
    public CollaboratorDTO entityToDto(Collaborator entity) {
        return new CollaboratorDTO(entity);
    }

}
