package com.example.demo.factories;

import com.example.demo.dtos.AbstractDTO;
import com.example.demo.entities.AbstractEntity;

public interface AbstractFactory<T extends AbstractEntity, TDTO extends AbstractDTO> {
    T dtoToEntity(TDTO dto);

    TDTO entityToDto(T entity);
}
