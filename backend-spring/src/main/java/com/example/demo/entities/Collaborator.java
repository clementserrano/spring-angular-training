package com.example.demo.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "collaborators")
public class Collaborator extends AbstractEntity {
    private String lastname;

    private String firstname;

    @ManyToOne
    @JoinColumn
    private Collaborator manager;

    @OneToMany(mappedBy = "manager")
    private Set<Collaborator> managed;

    private Boolean active;
}
