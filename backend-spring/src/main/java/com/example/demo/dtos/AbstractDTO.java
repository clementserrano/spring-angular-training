package com.example.demo.dtos;

import java.time.LocalDateTime;

import com.example.demo.entities.AbstractEntity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractDTO {
    private Long id;
    private LocalDateTime creationDate;
    private LocalDateTime updateDate;

    public AbstractDTO(AbstractEntity entity) {
        setId(entity.getId());
        setCreationDate(entity.getCreationDate());
        setUpdateDate(entity.getUpdateDate());
    }
}
