package com.example.demo.dtos;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.entities.AbstractEntity;
import com.example.demo.entities.Collaborator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CollaboratorDTO extends AbstractDTO {
    private String lastname;
    private String firstname;
    private Long managerId;
    private List<Long> managedIds;
    private Boolean active;

    public CollaboratorDTO(Collaborator entity) {
        super(entity);

        setLastname(entity.getLastname());
        setFirstname(entity.getFirstname());

        if (entity.getManager() != null) {
            setManagerId(entity.getManager().getId());
        }

        if (entity.getManaged() != null) {
            setManagedIds(entity.getManaged().stream().map(AbstractEntity::getId).collect(Collectors.toList()));
        }

        setActive(entity.getActive());
    }
}
