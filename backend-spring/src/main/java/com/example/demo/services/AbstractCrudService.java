package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dtos.AbstractDTO;
import com.example.demo.entities.AbstractEntity;
import com.example.demo.factories.AbstractFactory;

@Transactional
public abstract class AbstractCrudService<T extends AbstractEntity, TDTO extends AbstractDTO> {

    @Autowired
    private AbstractFactory<T, TDTO> factory;

    @Autowired
    private JpaRepository<T, Long> repository;

    public List<T> getAll() {
        return repository.findAll();
    }

    public Optional<T> get(Long id) {
        return repository.findById(id);
    }

    public T create(TDTO dto) {
        T entity = factory.dtoToEntity(dto);
        return repository.save(entity);
    }

    public T update(TDTO dto) {
        T entity = factory.dtoToEntity(dto);
        return repository.saveAndFlush(entity);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
