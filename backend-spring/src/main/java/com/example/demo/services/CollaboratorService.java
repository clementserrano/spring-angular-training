package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dtos.CollaboratorDTO;
import com.example.demo.entities.Collaborator;
import com.example.demo.repositories.CollaboratorRepository;

@Service
public class CollaboratorService extends AbstractCrudService<Collaborator, CollaboratorDTO> {

    @Autowired private CollaboratorRepository collaboratorRepository;
}
