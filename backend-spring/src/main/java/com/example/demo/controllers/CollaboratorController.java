package com.example.demo.controllers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dtos.CollaboratorDTO;
import com.example.demo.entities.Collaborator;
import com.example.demo.services.CollaboratorService;

@RestController
@RequestMapping(value = "/collaborators")
public class CollaboratorController extends AbstractCrudController<Collaborator, CollaboratorDTO> {

    @Autowired
    private CollaboratorService collaboratorService;

    @Override
    public ResponseEntity<List<CollaboratorDTO>> getAll(@RequestParam Map<String, String> params) {
        List<Collaborator> entities = collaboratorService.getAll(); // TODO parameter active
        List<CollaboratorDTO> dtos = entities.stream().map(entity -> factory.entityToDto(entity))
                .collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
