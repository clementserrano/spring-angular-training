package com.example.demo.controllers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dtos.AbstractDTO;
import com.example.demo.entities.AbstractEntity;
import com.example.demo.factories.AbstractFactory;
import com.example.demo.services.AbstractCrudService;

public abstract class AbstractCrudController<T extends AbstractEntity, TDTO extends AbstractDTO> {

    @Autowired
    protected AbstractCrudService<T, TDTO> service;

    @Autowired
    protected AbstractFactory<T, TDTO> factory;

    @GetMapping
    public ResponseEntity<List<TDTO>> getAll(@RequestParam Map<String, String> params) {
        List<T> entities = service.getAll();
        List<TDTO> dtos = entities.stream().map(entity -> factory.entityToDto(entity)).collect(Collectors.toList());
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TDTO> get(@PathVariable Long id) {
        T entity = service.get(id).orElseThrow(() -> new EntityNotFoundException());
        TDTO dto = factory.entityToDto(entity);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TDTO> create(@RequestBody TDTO dto) {
        T entity = service.create(dto);
        TDTO result = factory.entityToDto(entity);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<TDTO> update(@RequestBody TDTO dto) {
        T entity = service.update(dto);
        TDTO result = factory.entityToDto(entity);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
