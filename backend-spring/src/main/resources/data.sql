INSERT INTO collaborators (creation_date, update_date, lastname, firstname, manager_id, active) VALUES 
(NOW(), NOW(),'Neri', 'Gabrielle', null, true),
(NOW(), NOW(),'Lacroix', 'Dominique', 1, false),
(NOW(), NOW(),'Beaulne', 'Fernand', 1, true),
(NOW(), NOW(),'Peletier', 'Matthieu', 3, true),
(NOW(), NOW(),'Bruguière', 'Suzanne', 4, false);