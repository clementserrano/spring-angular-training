import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomHttpInterceptor } from './modules/core/interceptors/custom-http-interceptor';
import { AppInjector } from './modules/core/services/app-injector.service';
import { NavbarModule } from './modules/shared/navbar/navbar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NavbarModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    // Save injector ref in static util class AppInjector
    AppInjector.setInjector(injector);
  }
}
