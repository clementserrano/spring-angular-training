import { NgModule } from '@angular/core';
import { CollaboratorsRouter } from './collaborators.router';

@NgModule({
  imports: [
    CollaboratorsRouter
  ]
})
export class CollaboratorsModule { }
