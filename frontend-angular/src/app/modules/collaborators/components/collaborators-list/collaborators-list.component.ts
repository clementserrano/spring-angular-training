import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Collaborator } from 'src/app/modules/core/models/collaborator.model';
import { SortOrder } from 'src/app/modules/core/models/sort-order.model';

@Component({
  selector: 'app-collaborators-list',
  templateUrl: './collaborators-list.component.html',
  styleUrls: ['./collaborators-list.component.scss']
})
export class CollaboratorsListComponent implements OnInit {

  /**
   * Collaborators list to display
   */
  @Input() collaborators: Collaborator[];

  /**
   * Fired on collaborator row click
   */
  @Output() select = new EventEmitter<Collaborator>();

  /**
   * Order object to initialize sort
   */
  sortOrder: SortOrder = { column: 'firstname', order: 1 };

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Sort function, change column and order in sortOrder
   * If column was the same, reverse order
   * @param column column clicked in table
   */
  sort(column: string): void {
    this.sortOrder = {
      column,
      order: this.sortOrder?.column === column ? -this.sortOrder.order : 1
    };
  }
}
