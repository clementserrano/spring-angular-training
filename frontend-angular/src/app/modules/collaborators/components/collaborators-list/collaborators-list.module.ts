import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollaboratorsListComponent } from './collaborators-list.component';



@NgModule({
  declarations: [CollaboratorsListComponent],
  imports: [
    CommonModule
  ], exports: [CollaboratorsListComponent]
})
export class CollaboratorsListModule { }
