import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Collaborator } from 'src/app/modules/core/models/collaborator.model';
import { CollaboratorsService } from 'src/app/modules/core/services/collaborators.service';

@Component({
  selector: 'app-collaborator-form',
  templateUrl: './collaborator-form.component.html',
  styleUrls: ['./collaborator-form.component.scss']
})
export class CollaboratorFormComponent implements OnInit, OnChanges {

  /**
   * Collaborator with values to inject in form
   */
  @Input() collaborator: Collaborator;

  /**
   * True to disable form modifications and CRUD operations
   */
  @Input() disabled = false;

  /**
   * List of managers to populate manager select
   */
  @Input() managers: Collaborator[];

  /**
   * Form group for collaborator to track user inputs 
   */
  form: FormGroup;

  /**
   * Fired when collaborator is deleted in backend
   */
  @Output() deleted = new EventEmitter<Collaborator>();

  constructor(private formBuilder: FormBuilder, private collaboratorsService: CollaboratorsService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Init form group if undefined (Code in ngOnChanges to make sure form is created for following actions)
    if (!this.form) {
      this.form = this.formBuilder.group(
        {
          id: null,
          lastname: null,
          firstname: null,
          managerId: null
        }
      )
    }

    // Changes form group values on collaborator change
    if (changes['collaborator']) {
      this.reset();
    }
  }

  /**
   * Reset form with collaborator values
   */
  reset(): void {
    this.form.reset(this.collaborator);
  }

  /**
   * Perform create or update in backend, and update collaborator object with returned value
   */
  save(): void {
    const collaborator = this.form.getRawValue();
    const saveObs = this.collaborator.id ? this.collaboratorsService.update(collaborator) : this.collaboratorsService.create(collaborator);
    saveObs.subscribe({
      next: collaboratorSaved => Object.assign(this.collaborator, collaboratorSaved),
      error: err => console.error(err)
    });
  }

  /**
   * Delete collaborator in backend
   */
  delete() {
    this.collaboratorsService.delete(this.collaborator.id).subscribe({
      next: () => this.deleted.emit(this.collaborator),
      error: err => console.error(err)
    });
  }
}
