import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CollaboratorFormComponent } from './collaborator-form.component';



@NgModule({
  declarations: [CollaboratorFormComponent],
  imports: [
    CommonModule, ReactiveFormsModule
  ], exports: [CollaboratorFormComponent]
})
export class CollaboratorFormModule { }
