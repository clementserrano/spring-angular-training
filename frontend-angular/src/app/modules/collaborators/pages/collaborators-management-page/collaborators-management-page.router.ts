import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollaboratorsManagementPageComponent } from './collaborators-management-page.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: CollaboratorsManagementPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CollaboratorsManagementPageRouter { }