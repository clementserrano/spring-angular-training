import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CollaboratorFormModule } from '../../components/collaborator-form/collaborator-form.module';
import { CollaboratorsListModule } from '../../components/collaborators-list/collaborators-list.module';
import { CollaboratorsManagementPageComponent } from './collaborators-management-page.component';
import { CollaboratorsManagementPageRouter } from './collaborators-management-page.router';



@NgModule({
  declarations: [CollaboratorsManagementPageComponent],
  imports: [
    CommonModule, CollaboratorsManagementPageRouter, CollaboratorsListModule, CollaboratorFormModule, ReactiveFormsModule
  ]
})
export class CollaboratorsManagementPageModule { }
