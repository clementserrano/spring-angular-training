import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Collaborator } from 'src/app/modules/core/models/collaborator.model';
import { CollaboratorsService } from 'src/app/modules/core/services/collaborators.service';

@Component({
  selector: 'app-collaborators-management-page',
  templateUrl: './collaborators-management-page.component.html',
  styleUrls: ['./collaborators-management-page.component.scss']
})
export class CollaboratorsManagementPageComponent implements OnInit {

  /**
   * Collaborators list to pass to CollaboratorsListComponent
   */
  collaborators: Collaborator[];

  /**
   * Collaborator selected to pass to CollaboratorFormComponent
   */
  collaborator: Collaborator;

  /**
   * Form control to change boolean for disable form
   */
  disableForm: FormControl;

  constructor(private collaboratorsService: CollaboratorsService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    // Get all collaborators in backend
    this.collaboratorsService.getAll().subscribe(collaborators => {
      this.collaborators = collaborators;
    })

    // Init control disable to false
    this.disableForm = this.formBuilder.control(false);
  }

  /**
   * Change collaborator ref for form
   * @param collaborator collaborator clicked in list
   */
  select(collaborator: Collaborator) {
    this.collaborator = collaborator;
  }

  /**
   * Create new collaborator with empty values
   */
  create() {
    this.collaborator = new Collaborator();
  }

  /**
   * Delete collaborator in list by comparing with id
   * @param collaborator collaborator to delete
   */
  delete(collaborator: Collaborator) {
    const index = this.collaborators.findIndex(c => c.id === collaborator.id);
    if (index !== -1) {
      this.collaborators.splice(index, 1);
    }
  }
}
