import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorsManagementPageComponent } from './collaborators-management-page.component';

describe('CollaboratorsManagementPageComponent', () => {
  let component: CollaboratorsManagementPageComponent;
  let fixture: ComponentFixture<CollaboratorsManagementPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorsManagementPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CollaboratorsManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
