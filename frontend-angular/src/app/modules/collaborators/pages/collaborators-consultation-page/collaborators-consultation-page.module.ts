import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollaboratorsConsultationPageComponent } from './collaborators-consultation-page.component';
import { CollaboratorFormModule } from '../../components/collaborator-form/collaborator-form.module';
import { CollaboratorsListModule } from '../../components/collaborators-list/collaborators-list.module';
import { CollaboratorsConsultationPageRouter } from './collaborators-consultation-page.router';



@NgModule({
  declarations: [CollaboratorsConsultationPageComponent],
  imports: [
    CommonModule, CollaboratorsConsultationPageRouter, CollaboratorsListModule, CollaboratorFormModule
  ]
})
export class CollaboratorsConsultationPageModule { }
