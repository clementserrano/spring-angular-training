import { Component, OnInit } from '@angular/core';
import { Collaborator } from 'src/app/modules/core/models/collaborator.model';
import { CollaboratorsService } from 'src/app/modules/core/services/collaborators.service';

@Component({
  selector: 'app-collaborators-consultation-page',
  templateUrl: './collaborators-consultation-page.component.html',
  styleUrls: ['./collaborators-consultation-page.component.scss']
})
export class CollaboratorsConsultationPageComponent implements OnInit {

  /**
   * Collaborators list to pass to CollaboratorsListComponent
   */
  collaborators: Collaborator[];

  /**
   * Collaborator selected to pass to CollaboratorFormComponent
   */
  collaborator: Collaborator;

  constructor(private collaboratorsService: CollaboratorsService) { }

  ngOnInit(): void {
    // Get all collaborators in backend
    // TODO get collaborators active 
    this.collaboratorsService.getAll().subscribe(collaborators => {
      this.collaborators = collaborators;
    })
  }

  /**
   * Change collaborator ref for form
   * @param collaborator collaborator clicked in list
   */
  select(collaborator: Collaborator) {
    this.collaborator = collaborator;
  }
}
