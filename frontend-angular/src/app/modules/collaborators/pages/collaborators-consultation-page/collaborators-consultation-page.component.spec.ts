import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorsConsultationPageComponent } from './collaborators-consultation-page.component';

describe('CollaboratorsConsultationPageComponent', () => {
  let component: CollaboratorsConsultationPageComponent;
  let fixture: ComponentFixture<CollaboratorsConsultationPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollaboratorsConsultationPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CollaboratorsConsultationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
