import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollaboratorsConsultationPageComponent } from './collaborators-consultation-page.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: CollaboratorsConsultationPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CollaboratorsConsultationPageRouter { }