import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    // Default collaborators route to management page
    {
        path: '',
        pathMatch: 'full',
        loadChildren: () => import('./pages/collaborators-management-page/collaborators-management-page.module').then(m => m.CollaboratorsManagementPageModule),
    },
    {
        path: 'management',
        loadChildren: () => import('./pages/collaborators-management-page/collaborators-management-page.module').then(m => m.CollaboratorsManagementPageModule),
    },
    {
        path: 'consultation',
        loadChildren: () => import('./pages/collaborators-consultation-page/collaborators-consultation-page.module').then(m => m.CollaboratorsConsultationPageModule),
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CollaboratorsRouter { }