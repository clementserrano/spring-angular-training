export class AbstractEntity {
    id: number;
    creationDate: Date;
    updateDate: Date;
}