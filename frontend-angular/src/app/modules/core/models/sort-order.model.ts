export interface SortOrder {
    column: string;
    /**
     * 1 for ASC order
     * -1 for DESC order
     */
    order: number; 
}