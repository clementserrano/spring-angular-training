import { AbstractEntity } from "./abstract-entity.model";

export class Collaborator extends AbstractEntity {
    lastname: string;
    firstname: string;
    managerId: number;
    managedIds: number[];
    active: boolean;
}