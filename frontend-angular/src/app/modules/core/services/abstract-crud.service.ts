import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AbstractEntity } from '../models/abstract-entity.model';
import { AppInjector } from './app-injector.service';

/**
 * Abstract CRUD service to represent all backend enpoints
 */
export class AbstractCrudService<T extends AbstractEntity> {

  protected endpoint: string;
  protected http: HttpClient;

  constructor(route: string) {
    const injector = AppInjector.getInjector();
    this.http = injector.get(HttpClient);
    this.endpoint = `${environment.backend}/${route}`;
  }

  public getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.endpoint);
  }

  public get(id: number): Observable<T> {
    return this.http.get<T>(`${this.endpoint}/${id}`);
  }

  public create(entity: T): Observable<T> {
    return this.http.post<T>(this.endpoint, entity);
  }

  public update(entity: T): Observable<T> {
    return this.http.put<T>(this.endpoint, entity);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.endpoint}/${id}`);
  }
}
