import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Collaborator } from '../models/collaborator.model';
import { AbstractCrudService } from './abstract-crud.service';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorsService extends AbstractCrudService<Collaborator> {

  constructor() {
    super('collaborators');
  }

  /**
   * Same as super.getAll() but with filters param to filter collaborators
   * @param filters key value map for filtering
   * @returns observable of collaborators filtered
   */
  public getAllFilter(filters: { active: boolean }): Observable<Collaborator[]> {
    return this.http.get<Collaborator[]>(this.endpoint, { params: { ...filters } });
  }
}
